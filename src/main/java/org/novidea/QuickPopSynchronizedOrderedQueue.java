package org.novidea;

import java.util.Comparator;

public class QuickPopSynchronizedOrderedQueue<T> extends AbstractPopPushOrderedQueue<T> {
    public QuickPopSynchronizedOrderedQueue(int size, Comparator<T> comparator) {
        super(size, comparator);
    }

    public QuickPopSynchronizedOrderedQueue(Comparator<T> comparator) {
        super(comparator);
    }

    public synchronized void push(T element) {
        super.push(element);
        if (size() != 1) {
            heapifyUp(size());
        }
    }

    public synchronized T pop() {
        return super.pop();
    }

}
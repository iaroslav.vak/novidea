package org.novidea;

import java.util.Comparator;

public class QuickPushSynchronizedOrderedQueue<T> extends AbstractPopPushOrderedQueue<T> {

    private int heapifyFrom;

    public QuickPushSynchronizedOrderedQueue(int size, Comparator<T> comparator) {
        super(size, comparator);
    }

    public QuickPushSynchronizedOrderedQueue(Comparator<T> comparator) {
        super(comparator);
    }

    public synchronized void push(T element) {
        super.push(element);
        if (heapifyFrom == -1) {
            heapifyFrom = size();
        }
    }


    public synchronized T pop() {
        heapify();
        return super.pop();
    }

    private void heapify() {
        while (heapifyFrom != -1) {
            heapifyUp(heapifyFrom);
            heapifyFrom++;
            if (heapifyFrom == size()) {
                heapifyFrom = -1;
            }
        }
    }

}
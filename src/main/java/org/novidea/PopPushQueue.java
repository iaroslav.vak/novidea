package org.novidea;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public interface PopPushQueue<T> extends Iterable<T>{
    void push(T element);

    T pop();

    int size();

    default Stream<T> stream() {
        return StreamSupport.stream(spliterator(), false);
    }


}
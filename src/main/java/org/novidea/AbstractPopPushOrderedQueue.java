package org.novidea;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

public abstract class AbstractPopPushOrderedQueue<T> implements PopPushQueue<T> {
    private Object[] heap;
    private int size;
    private int maxsize;
    private Comparator<T> comparator;

    public AbstractPopPushOrderedQueue(int size, Comparator<T> comparator) {
        this.maxsize = size;
        this.size = 0;
        heap = new Object[this.maxsize + 1];
        this.comparator = comparator;
    }

    public AbstractPopPushOrderedQueue(Comparator<T> comparator) {
        this.maxsize = 10;
        this.size = 0;
        heap = new Object[this.maxsize + 1];
        this.comparator = comparator;
    }

    private int parent(int pos) {
        return pos / 2;
    }

    private int leftChild(int pos) {
        return (2 * pos);
    }

    private int rightChild(int pos) {
        return (2 * pos) + 1;
    }

    private void swap(int i1, int i2) {
        Object tmp;
        tmp = heap[i1];
        heap[i1] = heap[i2];
        heap[i2] = tmp;
    }

    protected void downHeapify(int i) {
        if (i != 1 && i >= (size / 2) && i <= size)
            return;
        if (size == 1) {
            return;
        }

        if (lessThanLeftChild(i) || rightChildExists(i) && lessThanRightChild(i)) {

            if (size == 2 || comparator.compare((T) heap[leftChild(i)], (T) heap[rightChild(i)]) > 0) {
                swap(i, leftChild(i));
                downHeapify(leftChild(i));
            } else {
                swap(i, rightChild(i));
                downHeapify(rightChild(i));
            }
        }
    }

    private boolean lessThanLeftChild(int i) {
        return comparator.compare((T) heap[i], (T) heap[leftChild(i)]) < 0;
    }

    private boolean lessThanRightChild(int i) {
        return comparator.compare((T) heap[i], (T) heap[rightChild(i)]) < 0;
    }

    private boolean rightChildExists(int i) {
        return rightChild(i) <= size;
    }

    protected void heapifyUp(int i) {
        Object temp = heap[i];
        while (i > 0 && parent(i) > 0 && comparator.compare((T) temp, (T) heap[parent(i)]) > 0) {
            heap[i] = heap[parent(i)];
            i = parent(i);
        }
        heap[i] = temp;
    }

    protected void grow() {
        maxsize = maxsize + (maxsize >> 1);
        heap = Arrays.copyOf(heap, maxsize + 1);
    }

    @Override
    public void push(T element) {
        if (element == null) {
            throw new IllegalArgumentException("null elements are forbidden");
        }
        if (heap[maxsize - 1] != null) {
            grow();
        }
        heap[++size] = element;
    }

    @Override
    public T pop() {
        if (size == 0) {
            throw new IndexOutOfBoundsException("The queue is empty");
        }
        Object max = heap[1];
        heap[1] = heap[size];
        heap[size--] = null;
        if (size != 0) {
            downHeapify(1);
        }
        return (T) max;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        return new PopPushQueueIterator<>(this);
    }

    private static class PopPushQueueIterator<E> implements Iterator<E> {
        private final PopPushQueue<E> q;

        PopPushQueueIterator(PopPushQueue<E> q) {
            this.q = q;
        }

        @Override
        public boolean hasNext() {
            return q.size() > 0;
        }

        @Override
        public E next() {
            return q.pop();
        }
    }
}
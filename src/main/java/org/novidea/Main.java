package org.novidea;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) throws IOException {
        try (BufferedReader is = new BufferedReader(new InputStreamReader(System.in))) {
            System.out.println("Choose implementation to test, type 'quickPop' or any other string to 'quickPush'");
            String implementation = is.readLine();
            PopPushQueue<Integer> popPushQueue = "quickPop".equals(implementation) ? new QuickPopSynchronizedOrderedQueue<>(Integer::compareTo)
                    : new QuickPushSynchronizedOrderedQueue<>(Integer::compareTo);

            while (true) {
                System.out.println("Type operation like 'pop', 'pop all' or 'push 4' to perform it.");
                System.out.println("You can separate integers with space to push more values at once 'push 4 2 3 4'");
                System.out.println("Or 'exit' to exit");
                String operation = is.readLine();
                if ("pop".equals(operation)) {
                    System.out.printf("Popped %s\n", popPushQueue.pop());
                } else if ("pop all".equals(operation)) {
                    System.out.println(popPushQueue.stream().toList());
                } else if ("exit".equals(operation)) {
                    return;
                } else {
                    String[] split = operation.split(" ");
                    if ("push".equals(split[0])) {
                        Stream.of(split).skip(1).map(Integer::valueOf).forEach(popPushQueue::push);
                    }
                }
            }
        }
    }
}

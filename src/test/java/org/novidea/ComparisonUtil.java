package org.novidea;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.IntStream;

public class ComparisonUtil {
    private static QuickPopSynchronizedOrderedQueue<Integer> popQueue = new QuickPopSynchronizedOrderedQueue<>(Integer::compareTo);
    private static QuickPushSynchronizedOrderedQueue<Integer> pushQueue = new QuickPushSynchronizedOrderedQueue<>(Integer::compareTo);

    public static void main(String[] args) {
        Random random = new Random();
        int size = 10000000;
        int[] array = IntStream.range(0, size).map(i -> random.nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE)).toArray();


        long timeBeforePushToPop = System.currentTimeMillis();
        Arrays.stream(array).forEach(popQueue::push);
        long timeAfterPushToPop = System.currentTimeMillis();
        System.out.printf("Millis of adding %s elements to PopQueue are %s%n", size, timeAfterPushToPop - timeBeforePushToPop);

        long timeBeforePushToPush = System.currentTimeMillis();
        Arrays.stream(array).forEach(pushQueue::push);
        long timeAfterPushToPush = System.currentTimeMillis();
        System.out.printf("Millis of adding %s elements to PushQueue are %s%n", size, timeAfterPushToPush - timeBeforePushToPush);

        long timeBeforePopFromPush = System.currentTimeMillis();
        pushQueue.pop();
        long timeAfterPopFromPush = System.currentTimeMillis();
        System.out.printf("Millis of popping first element from PushQueue are %s%n", timeAfterPopFromPush - timeBeforePopFromPush);

        long timeBeforePopFromPop = System.currentTimeMillis();
        popQueue.pop();
        long timeAfterPopFromPop = System.currentTimeMillis();
        System.out.printf("Millis of popping first element from PopQueue are %s%n", timeAfterPopFromPop - timeBeforePopFromPop);


        long timeBeforeSecondPopFromPop = System.currentTimeMillis();
        popQueue.pop();
        long timeAfterSecondPopFromPop = System.currentTimeMillis();
        System.out.printf("Millis of popping second element from PopQueue are %s%n", timeAfterSecondPopFromPop - timeBeforeSecondPopFromPop);

        long timeBeforeSecondPopFromPush = System.currentTimeMillis();
        pushQueue.pop();
        long timeAfterSecondPopFromPush = System.currentTimeMillis();
        System.out.printf("Millis of popping second element from PushQueue are %s%n", timeAfterSecondPopFromPush - timeBeforeSecondPopFromPush);

        long timeBeforePopAllFromPop = System.currentTimeMillis();
        while (popQueue.size() != 0) {
            popQueue.pop();
        }
        long timeAfterPopAllFromPop = System.currentTimeMillis();
        System.out.printf("Millis of popping all elements from PopQueue are %s%n", timeAfterPopAllFromPop - timeBeforePopAllFromPop);

        long timeBeforePopAllFromPush = System.currentTimeMillis();
        while (pushQueue.size() != 0) {
            pushQueue.pop();
        }
        long timeAfterPopAllFromPush = System.currentTimeMillis();
        System.out.printf("Millis of popping all elements from PushQueue are %s%n", timeAfterPopAllFromPush - timeBeforePopAllFromPush);
    }


}

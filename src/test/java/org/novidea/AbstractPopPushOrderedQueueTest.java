package org.novidea;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

abstract class AbstractPopPushOrderedQueueTest {
    protected abstract AbstractPopPushOrderedQueue<Integer> sut();

    @Test
    void testSizeAfterPush() {
        // given
        Assertions.assertEquals(0, sut().size());

        // when
        sut().push(10);
        sut().push(10);
        sut().push(11);
        sut().push(-11);
        sut().push(0);

        // then
        Assertions.assertEquals(5, sut().size());
    }

    @Test
    void testSizeAfterPop() {
        // given
        Assertions.assertEquals(0, sut().size());
        sut().push(10);
        sut().push(10);
        sut().push(11);
        sut().push(-11);
        sut().push(0);

        // when
        sut().pop();
        sut().pop();
        sut().pop();

        // then
        Assertions.assertEquals(2, sut().size());
    }
}
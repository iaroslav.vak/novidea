package org.novidea;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;


class QuickPopSynchronizedOrderedQueueTest extends AbstractPopPushOrderedQueueTest{

    private QuickPopSynchronizedOrderedQueue<Integer> sut;

    @BeforeEach
    void setUp() {
        sut = new QuickPopSynchronizedOrderedQueue<>(10, Integer::compareTo);
    }

    @Test
    void testPush() {
        // given
        Assertions.assertEquals(0, sut.size());
        int size = 101;
        int[] array = IntStream.range(0, size).toArray();

        // when
        Arrays.stream(array).forEach(sut::push);

        // then
        Assertions.assertEquals(size, sut.size());
    }

    @Test
    void pop() {
        // given
        Assertions.assertEquals(0, sut.size());
        Random random = new Random();
        int size = 11;
        int[] array = IntStream.range(0, size).map(i -> random.nextInt(Integer.MIN_VALUE, Integer.MAX_VALUE)).toArray();
        List<Integer> sortedList = Arrays.stream(array).mapToObj(i -> i).sorted(Comparator.comparingInt(Integer::intValue).reversed()).toList();

        // when
        Arrays.stream(array).forEach(sut::push);
        List<Integer> sutList = sut.stream().toList();

        // then
        Assertions.assertEquals(sortedList, sutList);
    }

    @Override
    protected AbstractPopPushOrderedQueue<Integer> sut() {
        return sut;
    }
}